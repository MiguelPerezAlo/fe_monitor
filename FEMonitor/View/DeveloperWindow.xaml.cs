﻿using FEMonitor.Helpers;
using FEMonitor.View;
using FEMonitor.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CommandInterpreter;

namespace FEMonitor
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DeveloperWindowViewModel vm = new DeveloperWindowViewModel();

        private List<string> CommandsHistory = new List<string>();
        private int RewindIndex = 0;
        private bool isHeaderShowing = true;
        private int currentFav = 0;
        private int totalFavs = 0;
        private int previousFav = -1;
        private object dummyNode = null;
        private Brush OSBackground;
        private Brush OSForeground;
        private FontFamily OSFontFam;
        private FontWeight OSFontWeight;
        private Brush OSCaretBrush;

        public MainWindow()
        {
            InitializeComponent();
            InitiliazeConsole();

            DataContext = vm;
        }

        private void InitiliazeConsole()
        {
            TerminalLabel.Content = "Terminal - Working directory: " + Manager.WorkingPath;

            DateTime localDate = DateTime.Now;

            TextBox tbx = (TextBox)FindName("TerminalTextBox");
            tbx.Text = " FE-Monitor developed by Miguel Pérez Alonso at Barcelona, Spain\n Local date and time: " + localDate.ToString() + "\n # ";
        }

        private void TerminalTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tbx = (TextBox)sender;
            int lastCommandIndex = tbx.Text.LastIndexOf('#') + 2;
            var sb = new StringBuilder(tbx.Text);

            if (e.Key == Key.Up)
            {
                if (RewindIndex > 0)
                {
                    sb.Remove(lastCommandIndex, (tbx.Text.Length - lastCommandIndex));
                    sb.Insert(lastCommandIndex, CommandsHistory[RewindIndex - 1]);
                    tbx.Text = sb.ToString();
                    RewindIndex--;
                }
                tbx.CaretIndex = tbx.Text.Length;
                e.Handled = true;
            }
            else if (e.Key == Key.Down)
            {
                if (RewindIndex < CommandsHistory.Count - 1)
                {
                    sb.Remove(lastCommandIndex, (tbx.Text.Length - lastCommandIndex));
                    sb.Insert(lastCommandIndex, CommandsHistory[RewindIndex + 1]);
                    tbx.Text = sb.ToString();
                    RewindIndex++;
                }
                tbx.CaretIndex = tbx.Text.Length;
                e.Handled = true;
            }

            else if (e.Key == Key.Left)
            {
                char[] commandline = tbx.Text.ToCharArray();
                if (commandline[tbx.CaretIndex - 2] == '#')
                {
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Back)
            {
                char[] commandline = tbx.Text.ToCharArray();
                if (commandline[tbx.CaretIndex - 2] == '#')
                {
                    e.Handled = true;
                }
                else if (isHeaderShowing)
                {

                }
            }
            else if (e.Key == Key.Enter)
            {
                string[] tbxContent = tbx.Text.Split('\n');
                string command = tbxContent[tbxContent.Length - 1];
                command = command.Replace("#", "");
                command = command.Trim(' ');
                CommandsHistory.Add(command);

                if (command.Contains("movefile"))
                {
                    string[] commandParts = command.Split('>');
                    string src = FirstCharToUpper(commandParts[0].Trim(' '));
                    string dest = FirstCharToUpper(commandParts[1].Split(' ')[3].Trim(' '));
                    string cmd = commandParts[1].Split(' ')[1].Trim(' ');
                    string file = commandParts[1].Split(' ')[2].Trim(' ');
                    command = "move " + ((Label)(FindName(src+"Label"))).Content + @"\" + file + " " + ((Label)(FindName(dest + "Label"))).Content;
                }
                else if (command.Contains("movebulk"))
                {
                    string[] commandParts = command.Split('>');
                    string src = FirstCharToUpper(commandParts[0].Trim(' '));
                    string dest = FirstCharToUpper(commandParts[1].Split(' ')[3].Trim(' '));
                    string cmd = commandParts[1].Split(' ')[1].Trim(' ');
                    string file = commandParts[1].Split(' ')[2].Trim(' ');
                    if(file.Contains("[") && file.Contains("]"))
                    {
                        string[] files = file.Split(',');
                        foreach (var obj in files)
                        {
                            command = "move " + ((Label)(FindName(src + "Label"))).Content + @"\" + obj.Trim('[').Trim(']').Trim(' ') + " " + ((Label)(FindName(dest + "Label"))).Content;
                            string singleResult = Manager.SendCommand(command);
                        }

                        tbx.Text += "\n Se han movido      " + files.Length + " objetos.\n";
                        tbx.Text += " # ";
                        tbx.CaretIndex = tbx.Text.Length;
                        RewindIndex = CommandsHistory.Count;
                        return;
                    }
                    else if (file.Contains("folders"))
                    {
                        Label srcLabel = (Label)(FindName(src + "Label"));
                        string[] directories = Directory.GetDirectories(srcLabel.Content.ToString());
                        foreach (var directory in directories)
                        {
                            command = "move " + directory + " " + ((Label)(FindName(dest + "Label"))).Content;
                            string singleResult = Manager.SendCommand(command);
                        }

                        tbx.Text += "\n Se han movido      " + directories.Length + " carpetas.\n";
                        tbx.Text += " # ";
                        tbx.CaretIndex = tbx.Text.Length;
                        RewindIndex = CommandsHistory.Count;
                        return;
                    }
                }
                else if (command.Contains("movezipping"))
                {
                    string[] commandParts = command.Split('>');
                    string src = FirstCharToUpper(commandParts[0].Trim(' '));
                    string dest = FirstCharToUpper(commandParts[1].Split(' ')[3].Trim(' '));
                    string cmd = commandParts[1].Split(' ')[1].Trim(' ');
                    string file = commandParts[1].Split(' ')[2].Trim(' ');
                }

                string result = Manager.SendCommand(command);

                TerminalLabel.Content = "Terminal - Working directory: " + Manager.WorkingPath;

                tbx.Text += "\n " + result;
                tbx.Text += " # ";
                tbx.CaretIndex = tbx.Text.Length;
                RewindIndex = CommandsHistory.Count;
            }
        }

        private void TerminalTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBox tbx = (TextBox)sender;
        }

        private void LightButton_Click(object sender, RoutedEventArgs e)
        {
            TerminalTextBox.Background = Brushes.White;
            TerminalTextBox.Foreground = Brushes.Black;
            TerminalTextBox.FontFamily = new FontFamily("Arial");
            TerminalTextBox.FontWeight = FontWeights.Normal;
            TerminalTextBox.CaretBrush = Brushes.Black;

            OldSchoolButton.Visibility = Visibility.Visible;
            LightButton.Visibility = Visibility.Hidden;
        }

        private void OldSchoolButton_Click(object sender, RoutedEventArgs e)
        {
            TerminalTextBox.Background = OSBackground;
            TerminalTextBox.Foreground = OSForeground;
            TerminalTextBox.FontFamily = OSFontFam;
            TerminalTextBox.FontWeight = OSFontWeight;
            TerminalTextBox.CaretBrush = OSCaretBrush;

            LightButton.Visibility = Visibility.Visible;
            OldSchoolButton.Visibility = Visibility.Hidden;
        }

        public string SelectedImagePath { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            int FavFolders = int.Parse(ConfigurationHelper.GetStringValue("FavFoldersIndex"));
            for (int i = 1; i < FavFolders + 1; i++)
            {
                string[] FavParts = ConfigurationHelper.GetStringValue("FavFolder" + i).Split(';');
                ((Label)FindName("Fav" + i + "Path")).Tag = FavParts[0];
                ((Label)FindName("Fav" + i + "Name")).Content = FavParts[1];
                ((StackPanel)FindName("Fav" + i)).Visibility = Visibility.Visible;
            }

            currentFav = FavFolders;
            totalFavs = FavFolders;

            foreach (string s in Directory.GetLogicalDrives())
            {
                if (s.Contains("D"))
                {
                    continue;
                }
                TreeViewItem item = new TreeViewItem();
                item.Header = s;
                item.Tag = s;
                item.FontWeight = FontWeights.Normal;
                item.Items.Add(dummyNode);
                item.Expanded += new RoutedEventHandler(folder_Expanded);
                if (Directory.GetDirectories(item.Tag.ToString()).Count() > 0)
                    foldersItem.Items.Add(item);
            }

            OSBackground =  TerminalTextBox.Background;
            OSForeground = TerminalTextBox.Foreground;
            OSFontFam = TerminalTextBox.FontFamily;
            OSFontWeight = TerminalTextBox.FontWeight;
            OSCaretBrush = TerminalTextBox.CaretBrush;
        }

        void folder_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if (item.Items.Count == 1 && item.Items[0] == dummyNode)
            {
                item.Items.Clear();
                try
                {
                    foreach (string s in Directory.GetDirectories(item.Tag.ToString()))
                    {
                        TreeViewItem subitem = new TreeViewItem();
                        subitem.Header = s.Substring(s.LastIndexOf("\\") + 1);
                        if (subitem.Header.ToString().IndexOf('$') != 0)
                        {
                            subitem.Tag = s;
                            subitem.FontWeight = FontWeights.Normal;
                            subitem.Items.Add(dummyNode);
                            subitem.Expanded += new RoutedEventHandler(folder_Expanded);
                            subitem.PreviewMouseDoubleClick += new MouseButtonEventHandler(OpenFolderInWindow);
                            subitem.PreviewMouseRightButtonDown += new MouseButtonEventHandler(OpenFolderContextualMenu);
                            item.Items.Add(subitem);
                        }
                    }
                }
                catch (Exception) { }
            }
        }

        private void OpenFolderContextualMenu(object sender, MouseButtonEventArgs e)
        {
            ContextMenu cm = this.FindResource("contextMenu") as ContextMenu;
            cm.PlacementTarget = sender as TreeViewItem;
            cm.Name = ((TreeViewItem)sender).Header.ToString();
            cm.Tag = ((TreeViewItem)sender).Tag.ToString();
            cm.IsOpen = true;
        }

        private void OpenFolderInWindow(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            TreeViewItem tvi = (TreeViewItem)sender;
            if (tvi.IsFocused)
            {
                OpenFolder(tvi.Tag.ToString());
            }
        }

        private void foldersItem_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView tree = (TreeView)sender;
            TreeViewItem temp = ((TreeViewItem)tree.SelectedItem);

            if (temp == null)
                return;
            SelectedImagePath = "";
            string temp1 = "";
            string temp2 = "";
            while (true)
            {
                temp1 = temp.Header.ToString();
                if (temp1.Contains(@"\"))
                {
                    temp2 = "";
                }
                SelectedImagePath = temp1 + temp2 + SelectedImagePath;
                if (temp.Parent.GetType().Equals(typeof(TreeView)))
                {
                    break;
                }
                temp = ((TreeViewItem)temp.Parent);
                temp2 = @"\";
            }
            //show user selected path
            //MessageBox.Show(SelectedImagePath);
        }

        private void TabItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
            var settings = appSettings.Settings;
            string[] tabs = settings["AvailableTabs"].Value.Split(',');

            TabItem item = (TabItem)sender;
            int index = Array.IndexOf(tabs, item.Header);

            ChangeNameWindow cnw = new ChangeNameWindow(item.Header.ToString());
            cnw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            cnw.ShowDialog();

            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            appSettings = (AppSettingsSection)config.GetSection("appSettings");
            settings = appSettings.Settings;
            tabs = settings["AvailableTabs"].Value.Split(',');

            item.Header = tabs[index];
        }

        private void window1_Drop(object sender, DragEventArgs e)
        {

        }

        private Point startPoint;

        private void tvPreviewLeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private void tvPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
                return;

            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
                Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
            {
                TreeView treeView = sender as TreeView;
                TreeViewItem treeViewItem = FindAnchestor<TreeViewItem>((DependencyObject)e.OriginalSource);
                object val = treeView.ItemContainerGenerator.ItemFromContainer(treeViewItem);
                DataObject dragData = new DataObject("myFormat", treeViewItem.Header);
                DragDrop.DoDragDrop(treeViewItem, dragData, DragDropEffects.Move);
            }
        }

        private static T FindAnchestor<T>(DependencyObject current)
            where T : DependencyObject
        {
            do
            {
                if (current is T)
                    return (T)current;
                current = VisualTreeHelper.GetParent(current);
            } while (current != null);
            return null;
        }

        private void frtbDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("myFormat"))
            {
                var val = (TreeViewItem)e.Data.GetData("myFormat");
                WebBrowser window = (WebBrowser)FindName("window1Content");
                window.Navigate((string)val.Tag);
            }
        }

        private void frtbDragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("myFormat") || sender == e.Source)
                e.Effects = DragDropEffects.None;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            string WindowRootName = ((Button)sender).Name.Replace("Close", "");
            string WindowToClose = WindowRootName + "Content";
            string WindowHeader = WindowRootName + "Header";

            ((Label)FindName(WindowRootName + "Label")).Content = "";
            ((Border)FindName(WindowHeader)).Visibility = Visibility.Hidden;
            ((WebBrowser)FindName(WindowToClose)).Navigate((Uri)null);

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            currentFav++;
            totalFavs++;
            if (totalFavs > 10)
            {
                MessageBox.Show("Max favourites folders reached");
                return;
            }
            ContextMenu cm = this.FindResource("contextMenu") as ContextMenu;

            ((Label)FindName("Fav" + currentFav + "Path")).Tag = cm.Tag;
            ((Label)FindName("Fav" + currentFav + "Name")).Content = cm.Name;
            ((StackPanel)FindName("Fav" + currentFav)).Visibility = Visibility.Visible;

            ConfigurationHelper.SetStringValue("FavFoldersIndex", totalFavs.ToString());
            ConfigurationHelper.SetStringValue("FavFolder" + currentFav, cm.Tag + ";" + cm.Name);

            if (previousFav != -1)
            {
                currentFav = previousFav;
                previousFav = -1;
            }
        }

        private void OpenFav(object sender, MouseButtonEventArgs e)
        {
            string FavRoot = ((StackPanel)sender).Name;
            string FavPath = ((Label)FindName(FavRoot + "Path")).Tag.ToString();

            OpenFolder(FavPath);

        }

        private void OpenFolder(string Path)
        {
            IEnumerable<WebBrowser> ConfigurableWindows = GeneralHelpers.FindVisualChildren<WebBrowser>(this).Where(x => x.Tag != null && x.Tag.ToString() == "ConfigurableWindow");
            WebBrowser freeWindow = ConfigurableWindows.FirstOrDefault(window => window.Source == null);
            if (freeWindow == null)
            {
                if (TabMenu.SelectedIndex + 1 < TabMenu.Items.Count)
                {
                    TabMenu.SelectedIndex++;
                    TabMenu.UpdateLayout();
                    OpenFolder(Path);
                }
                else
                {
                    MessageBox.Show("NO FREE WINDOWS");
                }
            }
            else
            {
                freeWindow.Source = new Uri(Path);
                ((Border)FindName(freeWindow.Name.Replace("Content", "Header"))).Visibility = Visibility.Visible;
                ((Label)FindName(freeWindow.Name.Replace("Content", "Label"))).Content = Path;
            }
        }

        private void FavItem_Click(object sender, RoutedEventArgs e)
        {
            ContextMenu cm = FindResource("favMenu") as ContextMenu;
            previousFav = currentFav;
            currentFav = int.Parse(cm.Name.Replace("Fav", "")) - 1;
            totalFavs--;
            ((StackPanel)FindName(cm.Name)).Visibility = Visibility.Hidden;
        }

        private void RightClick_FavItem(object sender, MouseButtonEventArgs e)
        {
            ContextMenu cm = this.FindResource("favMenu") as ContextMenu;
            cm.PlacementTarget = sender as StackPanel;
            cm.Name = ((StackPanel)sender).Name;
            cm.IsOpen = true;
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D && Keyboard.Modifiers == ModifierKeys.Control)
            {
                UserWindow mw = new UserWindow();
                mw.Show();
                Window window = (Window)sender;
                window.Close();
            }
        }

        private string FirstCharToUpper(string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Image_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
