﻿using FEMonitor.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FEMonitor.View
{
    /// <summary>
    /// Lógica de interacción para ChangeNameWindow.xaml
    /// </summary>
    ///
    public partial class ChangeNameWindow : Window
    {
        public static string PreviousName { set; get; }
        ChangeNameWindowViewModel vm;
        public ChangeNameWindow(string prevName)
        {
            PreviousName = prevName;
            vm = new ChangeNameWindowViewModel();
            DataContext = vm;
            InitializeComponent();
        }
    }
}
