﻿using FEMonitor.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;

namespace FEMonitor.ViewModel
{
    class UserWindowViewModel : ObservableObject
    {
        private Window _window;
        private int _OuterMarginSize = 10;
        private int _WindowRadius = 10;
        static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        static AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
        static KeyValueConfigurationCollection settings = appSettings.Settings;
        static string[] tabs = settings["AvailableTabs"].Value.Split(',');

        public UserWindowViewModel(Window window)
        {
            _window = window;
            _window.StateChanged += (sender, e) =>
            {
                OnPropertyChanged(nameof(ResizeBorderThickness));
                OnPropertyChanged(nameof(OuterMarginSize));
                OnPropertyChanged(nameof(OuterMarginSizeThickness));
                OnPropertyChanged(nameof(WindowRadius));
                OnPropertyChanged(nameof(WindowCornerRadius));
            };
        }

        private string _firstTab = tabs[0];
        public string FirstTab
        {
            get { return _firstTab; }
            set
            {
                _firstTab = value;
            }
        }

        private string _secondTab = tabs[1];
        public string SecondTab
        {
            get { return _secondTab; }
            set
            {
                _secondTab = value;
            }
        }

        private string _thirdTab = tabs[2];
        public string ThirdTab
        {
            get { return _thirdTab; }
            set
            {
                _thirdTab = value;
            }
        }

        public int ResizeBorder { get; set; } = 6;
        public Thickness ResizeBorderThickness { get { return new Thickness(ResizeBorder + OuterMarginSize); } }

        public int OuterMarginSize
        {
            get => _window.WindowState == WindowState.Maximized ? 0 : _OuterMarginSize;
            set => _OuterMarginSize = value;
        }

        public Thickness OuterMarginSizeThickness { get { return new Thickness(OuterMarginSize); } }

        public int WindowRadius
        {
            get => _window.WindowState == WindowState.Maximized ? 0 : _WindowRadius;
            set => _WindowRadius = value;
        }

        public CornerRadius WindowCornerRadius { get { return new CornerRadius(WindowRadius); } }

        public int TitleHeight { get; set; } = 42;
    }
}
