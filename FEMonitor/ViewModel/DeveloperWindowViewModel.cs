﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace FEMonitor.ViewModel
{
    class DeveloperWindowViewModel
    {
        static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        static AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
        static KeyValueConfigurationCollection settings = appSettings.Settings;
        static string[] tabs = settings["AvailableTabs"].Value.Split(',');

        private string _firstTab = tabs[0];
        public string FirstTab
        {
            get { return _firstTab; }
            set
            {
                _firstTab = value;
            }
        }

        private string _secondTab = tabs[1];
        public string SecondTab
        {
            get { return _secondTab; }
            set
            {
                _secondTab = value;
            }
        }

        private string _thirdTab = tabs[2];
        public string ThirdTab
        {
            get { return _thirdTab; }
            set
            {
                _thirdTab = value;
            }
        }
    }
}
