﻿using FEMonitor.Helpers;
using FEMonitor.View;
using System;
using System.Configuration;
using System.Windows;
using System.Windows.Input;

namespace FEMonitor.ViewModel
{
    class ChangeNameWindowViewModel : ObservableObject
    {
        private string _newName = ChangeNameWindow.PreviousName;
        public string NewName
        {
            get { return _newName; }
            set
            {
                _newName = value;
                RaisePropertyChanged("NewName");
            }
        }
        bool CanSave(Window window)
        {
            return NewName != string.Empty;
        }
        void SaveExec(Window window)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
            var settings = appSettings.Settings;
            string[] tabs = settings["AvailableTabs"].Value.Split(',');
            int index = Array.IndexOf(tabs, ChangeNameWindow.PreviousName);
            tabs[index] = NewName;
            settings["AvailableTabs"].Value = string.Join(",", tabs);

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            window.Close();
        }
        public ICommand SaveNameCmd { get { return new RelayCommand<Window>(SaveExec, CanSave); } }
    }
}
