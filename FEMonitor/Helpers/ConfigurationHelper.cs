﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEMonitor.Helpers
{
    class ConfigurationHelper
    {
        public static void SetStringValue(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            AppSettingsSection appSettings = config.AppSettings;
            appSettings.Settings[key].Value = value;

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static string GetStringValue(string key)
        {
            string result = string.Empty;

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            AppSettingsSection appSettings = config.AppSettings;
            if (appSettings.Settings[key] != null)
            {
                result = appSettings.Settings[key].Value;
            }

            return result;
        }

    }
}
